function diemKhuVuc(KhuVuc){
    switch(KhuVuc) {
        case "A":
            return 2;
        case "B":
            return 1;
        case "C":
            return 0.5;
        default:
      }
    return 0;
}

function diemDoiTuong(doiTuong){
    switch(doiTuong) {
        case 1:
            return 2.5;
        case 2:
            return 1.5;
        case 3:
            return 1;
        default:
      }
    return 0;
}

function processB1(){
    var khuVuc = document.getElementById("khuVuc").value;
    var doiTuong = document.getElementById("doiTuong").value*1;
    var diemChuan = document.getElementById("diemChuan").value;
    var diem1 = document.getElementById("diem1").value*1;
    var diem2 = document.getElementById("diem2").value*1;
    var diem3 = document.getElementById("diem3").value*1;
    var total = diem1+diem2+diem3+diemKhuVuc(khuVuc)+diemDoiTuong(doiTuong);
    console.log(total);
    if (diem1*diem2*diem3!=0&&total>=diemChuan){
        document.getElementById("resultB1").innerText=`Thi sinh đã đậu, điểm của thí sinh ${total}`
    }
    else{
        document.getElementById("resultB1").innerText=`Thi sinh đã được 1 vé năm sau thi lại`
    }
}